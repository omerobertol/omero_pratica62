
import java.util.List;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

public class Pratica62 {

  public static void main(String[] args) {
    Time time = new Time();
    
    // adicionando jogadores ao time
    time.addJogador("Goleiro", new Jogador(1, "Fulano"));
    time.addJogador("Zagueiro", new Jogador(2, "João"));
    time.addJogador("Zagueiro2", new Jogador(3, "Mário"));
    time.addJogador("Zagueiro3", new Jogador(4, "Mário"));
    time.addJogador("Lateral", new Jogador(5, "Ciclano"));
    time.addJogador("Atacante", new Jogador(6, "Beltrano"));
    time.addJogador("Atacante2", new Jogador(7, "José"));
    time.addJogador("Atacante3", new Jogador(8, "José"));
    time.addJogador("Atacante4", new Jogador(9, "José"));
    time.addJogador("Atacante5", new Jogador(10, "José"));
    time.addJogador("Atacante6", new Jogador(11, "José"));
    
    // ordem descendente de nome e ascendente de número
    JogadorComparator ordem = new JogadorComparator(false, true, false);
    List<Jogador> timeA = time.ordena(ordem);  
    
    timeA.forEach((jogador) -> {
        System.out.println(jogador.toString());
      });
  }
    
}
