package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Time {
    
  private final HashMap<String, Jogador> jogadores;

  public Time() {
    this.jogadores = new HashMap<>();
  }

  public HashMap<String, Jogador> getJogadores() {
    return jogadores;
  }
  
  public void addJogador(String pos, Jogador jog) {
    this.jogadores.put(pos, jog);
  }
  
  public List<Jogador> ordena(JogadorComparator tipo) {
    List<Jogador> time = new ArrayList<>(this.jogadores.values()); 
    
    Collections.sort(time, tipo);
    
    return time;
  }  
    
}