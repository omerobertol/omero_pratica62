package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

public class JogadorComparator implements Comparator<Jogador> {
    private final boolean porNumero;
    private final int ordemNumero;
    private final int ordemNome;
      
    // O construtor padrão deve ser equivalente a ordenar por número e nome em ordem ascendente.
    public JogadorComparator() {
      porNumero = true;  // ordena por número e nome 
      ordemNumero = +1;  // número por ordem ascendente
      ordemNome = +1;    // nome por ordem ascendente
    }
 
    // - primeiro: ordenar por número (true), caso contrário, será por nome (false). 
    // - segundo e terceiro argumentos indicam se a respectiva ordenação será em 
    //   ordem ascendente (true) ou descendente (false).
    public JogadorComparator(boolean primeiro, boolean segundo, boolean terceiro) {
      porNumero = primeiro;
      
      if (segundo == true)
          ordemNumero = +1;     // ordem ascendente
      else ordemNumero = -1;    // ordem descendente
      
      if (terceiro == true)
          ordemNome = +1;
      else ordemNome = -1;
    }    

    @Override
    public int compare(Jogador o1, Jogador o2) {
      if (porNumero == true) // número e nome
         if (o1.numero == o2.numero)
            // comparando por número, se forem iguais, comparar pelo nome 
            if (o1.nome.equalsIgnoreCase(o2.nome))
                return(0);
            else return((o1.nome.compareToIgnoreCase(o2.nome)) * ordemNome);
         else return((o1.numero - o2.numero) * ordemNumero);
      else // nome e número  
        if (o1.nome.equalsIgnoreCase(o2.nome))
           // comparando por nome, se forem iguais, comparar pelo número
           return((o1.numero - o2.numero) * ordemNumero);
        else return((o1.nome.compareToIgnoreCase(o2.nome)) * ordemNome);    
    }

}